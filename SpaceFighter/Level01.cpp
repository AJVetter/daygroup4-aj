

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 21;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	/*
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};
	*/

	// good luck with these spawn times
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		1.0, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		1.25, 0.25, 0.25, 0.25, 0.25,
		1.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 3.0; // start delay
	Vector2 position;

	// loop for the amount of enemies
	for (int i = 0; i < COUNT; i++)
	{
		// add a delay based on how many iterations
		delay += delays[i];

		// apply the position
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		// setup the enemy
		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);

		// add enemy to game
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

